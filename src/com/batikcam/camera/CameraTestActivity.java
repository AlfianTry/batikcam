package com.batikcam.camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theveganrobot.OpenASURF.Crop;
import com.ugm.batikcam.MainActivity;
import com.ugm.batikcam.R;

/* Import ZBar Class files */

public class CameraTestActivity extends Activity {

	public int status = 0;
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	protected static final String TAG = null;
	protected static final Throwable e = null;
	Rect rect = new Rect(0, 0, 480, 800);
	Bitmap gambar = null;
	TextView scanText;
	Button scanButton;
	Bitmap bitmap;
	

	private boolean previewing = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_camera);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();
		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);
		
		
		
		scanButton = (Button) findViewById(R.id.ScanButton);
		getActionBar().hide();

	}

	@Override
	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	public void releaseCamera() {
		if (mCamera != null) {
			mPreview.getHolder().removeCallback(mPreview);
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		@Override
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {

		@Override
		public void onPreviewFrame(final byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			List<Size> sizes = parameters.getSupportedPictureSizes();
	        Camera.Size  result = null;

	        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
	        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();

	        for (int i=0;i<sizes.size();i++){
	            result = sizes.get(i);
	            arrayListForWidth.add(result.width);
	            arrayListForHeight.add(result.height);
	            Log.d("PictureSize", "Supported Size: " + result.width + "height : " + result.height);  
	            Log.d("AAAA","BACK PictureSize Supported Size: " + result.width + "height : " + result.height);  
	        } 
	        if(arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0){
	            System.out.println("Back max W :"+Collections.max(arrayListForWidth));              // Gives Maximum Width
	            System.out.println("Back max H :"+Collections.max(arrayListForHeight));                 // Gives Maximum Height
	            
	        }
	        
	        //Toaster("Back Megapixel :"+( ((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1000 ) );
	        
			final Size size = parameters.getPreviewSize();

			scanButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					//mPreview.takePicture();
					scanButton.setClickable(false);
					

						previewing = false;
						mCamera.setPreviewCallback(null);

						final YuvImage image = new YuvImage(data,
								ImageFormat.NV21, size.width, size.height, null);
						ByteArrayOutputStream os = new ByteArrayOutputStream(
								data.length);
						if (!image.compressToJpeg(new Rect(0, 0, size.width,
								size.height), 100, os)) {
							return;
						}
						byte[] tmp = os.toByteArray();
						
						Bitmap bmp = BitmapFactory.decodeByteArray(tmp, 0,
								tmp.length);
						//double a = size.width*size.height;
						//Toast.makeText(getApplicationContext(),String.valueOf(a),Toast.LENGTH_LONG).show();
						Matrix matrix = new Matrix();
						matrix.postRotate(90);
						bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
								bmp.getHeight(), matrix, true);
						onPause();
						saveImageToExternalStorage(bmp, "original.jpg");
						
						Intent nextScreen = new Intent(getApplicationContext(),
								Crop.class);
						
						startActivity(nextScreen);

					
				}

			});

		}
	};
	
	public void Toaster(String a){
		Toast.makeText(getApplicationContext(), a, Toast.LENGTH_LONG).show();
	}

	public void saveImageToExternalStorage(Bitmap image, String a) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/myid";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());
			String imageFileName = a;
			File file = new File(fullPath, imageFileName);
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			Log.e("saveToExternalStorage()", e.getMessage());
		}
	}

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent toCamera = new Intent(CameraTestActivity.this,
				MainActivity.class);
		startActivity(toCamera);
	}
	
	class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	    private SurfaceHolder mHolder;
	    private Camera mCamera;
	    private PreviewCallback previewCallback;
	    private AutoFocusCallback autoFocusCallback;

	    public CameraPreview(Context context, Camera camera,
	                         PreviewCallback previewCb,
	                         AutoFocusCallback autoFocusCb) {
	        super(context);
	        mCamera = camera;
	        previewCallback = previewCb;
	        autoFocusCallback = autoFocusCb;

	        /* 
	         * Set camera to continuous focus if supported, otherwise use
	         * software auto-focus. Only works for API level >=9.
	         */
	        /*
	        Camera.Parameters parameters = camera.getParameters();
	        for (String f : parameters.getSupportedFocusModes()) {
	            if (f == Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
	                mCamera.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
	                autoFocusCallback = null;
	                break;
	            }
	        }
	        */

	        // Install a SurfaceHolder.Callback so we get notified when the
	        // underlying surface is created and destroyed.
	        mHolder = getHolder();
	        mHolder.addCallback(this);

	        // deprecated setting, but required on Android versions prior to 3.0
	        /*mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);*/
	    }

	    /*public void surfaceCreated(SurfaceHolder holder) {
	        // The Surface has been created, now tell the camera where to draw the preview.
	        try {
	            mCamera.setPreviewDisplay(holder);
	        } catch (IOException e) {
	            Log.d("DBG", "Error setting camera preview: " + e.getMessage());
	        }
	    }*/

	    @Override
		public void surfaceDestroyed(SurfaceHolder holder) {
	        // Camera preview released in activity
	    	
	    }

	    @Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        /*
	         * If your preview can change or rotate, take care of those events here.
	         * Make sure to stop the preview before resizing or reformatting it.
	         */
	        if (mHolder.getSurface() == null){
	          // preview surface does not exist
	          return;
	        }

	        // stop preview before making changes
	        try {
	            mCamera.stopPreview();
	        } catch (Exception e){
	          // ignore: tried to stop a non-existent preview
	        }

	        try {
	            // Hard code camera surface rotation 90 degs to match Activity view in portrait
	            mCamera.setDisplayOrientation(90);

	            mCamera.setPreviewDisplay(mHolder);
	            mCamera.setPreviewCallback(previewCallback);
	            mCamera.startPreview();
	            mCamera.autoFocus(autoFocusCallback);
	        } catch (Exception e){
	            Log.d("DBG", "Error starting camera preview: " + e.getMessage());
	        }
	    }

		@Override
		public void surfaceCreated(SurfaceHolder arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			if(event.getAction() == MotionEvent.ACTION_DOWN){
				//Log.d("HALO", "EEK");
			}
			return super.onTouchEvent(event);
		}
		
	}
	
}
