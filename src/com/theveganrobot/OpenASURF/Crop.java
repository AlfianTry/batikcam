 package com.theveganrobot.OpenASURF;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.pedant.SweetAlert.SweetAlertDialog;

import com.edmodo.cropper.CropImageView;
import com.theveganrobot.OpenASURF.SurfLib.SurfInfo;
import com.theveganrobot.OpenASURF.swig.IpPairVector;
import com.ugm.batikcam.MainActivity;
import com.ugm.batikcam.R;
import com.ugm.batikcam.Result;
import com.ugm.batikcam.SplashActivity;

/*import com.googlecode.leptonica.android.*;
 import com.googlecode.tesseract.android.TessBaseAPI;*/

public class Crop extends Activity {

	private static final int SURF_PROGRESS_BAR = 1;
	public static final int SURF_INTERUPTED = 2;
	public static final int SURF_FAILED = 3;
	private static final int DEFAULT_ASPECT_RATIO_VALUES = 1;
	private static final String ASPECT_RATIO_X = "ASPECT_RATIO_X";
	private static final String ASPECT_RATIO_Y = "ASPECT_RATIO_Y";

	// Instance variables
	private int mAspectRatioX = DEFAULT_ASPECT_RATIO_VALUES;
	private int mAspectRatioY = DEFAULT_ASPECT_RATIO_VALUES;
	public String str;
	public Bitmap croppedImage;
	private Bitmap mBitmap;
	public Bitmap crop2;
	public Bitmap kawung2;
	public int cropint;

	IpPairVector kawung_match;
	IpPairVector kawung_match2;
	Long matcher;

	static {
		try {
			System.loadLibrary("opencv");
			System.loadLibrary("OpenSURF");
			System.loadLibrary("opencv_java");

		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateDialog(int)
	 */
	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {
		case SURF_INTERUPTED: {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("You interupted the RECOGNIZING process!")
					.setCancelable(false)
					.setPositiveButton("Sorry...",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									
									

								}
							});
			AlertDialog alert = builder.create();
			return alert;
		}

		case SURF_PROGRESS_BAR: {
			
			ProgressDialog progres = new ProgressDialog(this, AlertDialog.THEME_HOLO_LIGHT);
			progres.setMessage("Memproses");
			progres.getWindow().setBackgroundDrawableResource(
		            R.color.transparent);
			progres.show();
			return progres;
		}
		
		case SURF_FAILED: {

			SweetAlertDialog sweet = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
			sweet.setTitle("Oopps...");
			sweet.setContentText("Batik Tidak Dapat Dikenali");
			sweet.setTitleText("Maaf");
			sweet.showCancelButton(true);
			sweet.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
		        @Override
		        public void onClick(SweetAlertDialog sDialog) {
		            sDialog.dismiss();
		            Intent i = new Intent(Crop.this,MainActivity.class);
					startActivity(i);
		        }
		    });
			sweet.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {				
				
				public String nama = "";
				public String kegunaan = "";
				public String filosofi = "";

				
				@Override
				public void onClick(SweetAlertDialog sweetAlertDialog) {
					// TODO Auto-generated method stub
					
					AlertDialog.Builder alert = new AlertDialog.Builder(Crop.this, AlertDialog.THEME_HOLO_LIGHT);

					LinearLayout layout = new LinearLayout(Crop.this);
					layout.setOrientation(LinearLayout.VERTICAL);
					
					final TextView blanks = new TextView(Crop.this);
					blanks.setText("    ");
					blanks.setTextSize(10f);
					blanks.setGravity(Gravity.CENTER_HORIZONTAL);
					layout.addView(blanks);
					
					final TextView title = new TextView(Crop.this);
					title.setText("Masukkan Detail Batik");
					title.setTextSize(20f);
					title.setTextColor(Color.BLACK);
					title.setGravity(Gravity.CENTER_HORIZONTAL);
					layout.addView(title);
					
					final TextView blank = new TextView(Crop.this);
					blank.setText("    ");
					blank.setTextSize(18f);
					blank.setGravity(Gravity.CENTER_HORIZONTAL);
					layout.addView(blank);
					
					final EditText titleBox = new EditText(Crop.this);
					titleBox.setHint("Nama Motif");
					titleBox.setTextColor(Color.BLACK);
					layout.addView(titleBox);

					final EditText descriptionBox = new EditText(Crop.this);
					descriptionBox.setHint("Kegunaan");
					descriptionBox.setTextColor(Color.BLACK);
					layout.addView(descriptionBox);
					
					final EditText descriptionBox1 = new EditText(Crop.this);
					descriptionBox1.setHint("Filosofi");
					descriptionBox1.setTextColor(Color.BLACK);
					layout.addView(descriptionBox1);
					
					alert.setView(layout);

					alert.setPositiveButton("Tambah", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						String timeStamp = new SimpleDateFormat("yyD_HHmmss")
						.format(new Date());
						
						nama = titleBox.getText().toString();
						saveImageToExternalStorages(croppedImage, nama);
						kegunaan = descriptionBox.getText().toString();
						filosofi = descriptionBox1.getText().toString();
		
						UploadFile upload = new UploadFile();
						upload.postImage(nama + "_" + "update" + "_" + timeStamp + ".jpg",nama,kegunaan,filosofi,Crop.this);
						dialog.dismiss();
			            Intent i = new Intent(Crop.this,MainActivity.class);
						startActivity(i);
					  }
					});

					alert.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
					  @Override
					public void onClick(DialogInterface dialog, int whichButton) {
					    // Canceled.
						    dialog.dismiss();
				            Intent i = new Intent(Crop.this,MainActivity.class);
							startActivity(i);
					  }
					});

					alert.show();
				}
			}); 
			sweet.show();
			return sweet;
		}

		default:
			return super.onCreateDialog(id);
		}

	}

	// Restores the state upon rotating the screen/restarting the activity
	@Override
	protected void onRestoreInstanceState(Bundle bundle) {
		super.onRestoreInstanceState(bundle);
		mAspectRatioX = bundle.getInt(ASPECT_RATIO_X);
		mAspectRatioY = bundle.getInt(ASPECT_RATIO_Y);
	}
	
	public void saveImageToExternalStorages(Bitmap image, String nama_batik) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/all_batik";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyD_HHmmss")
					.format(new Date());
			String imageFileName = nama_batik + "_" + "update" + "_" + timeStamp + ".jpg";
			String str = imageFileName;
			File file = new File(fullPath, imageFileName);
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			Log.e("saveToExternalStorage()", e.getMessage());
		}
	}
	
	
	public void saveImageToExternalStorage(Bitmap image, String nama_batik) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/all_batik";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyD_HHmmss")
					.format(new Date());
			String imageFileName = nama_batik + "_" + timeStamp + ".jpg";
			String str = imageFileName;
			File file = new File(fullPath, imageFileName);
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			Log.e("saveToExternalStorage()", e.getMessage());
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {

		return surfedbitmap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPrepareDialog(int, android.app.Dialog)
	 */
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		// TODO Auto-generated method stub
		super.onPrepareDialog(id, dialog);
	}

	Bitmap surfedbitmap;
	Bitmap kawung;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_crop);

		String gaktau = "original.jpg";
		String ExternalStorageDirectoryPath = Environment
				.getExternalStorageDirectory().getAbsolutePath();
		final File file = new File(ExternalStorageDirectoryPath + "/myid/"
				+ gaktau);

		kawung = BitmapFactory.decodeResource(getResources(),
				R.drawable.kawung_cakra);
		// Pix pix_kawung = ReadFile.readBitmap(kawung);
		// pix_kawung = Binarize.otsuAdaptiveThreshold(pix_kawung);

		// Initialize components of the app
		final CropImageView cropImageView = (CropImageView) findViewById(R.id.CropImageView);

		// Sets initial aspect ratio to 10/10, for demonstration purposes
		cropImageView.setAspectRatio(DEFAULT_ASPECT_RATIO_VALUES,
				DEFAULT_ASPECT_RATIO_VALUES);
		try {
			mBitmap = BitmapFactory.decodeStream(getContentResolver()
					.openInputStream(Uri.fromFile(file)));
			cropImageView.setImageBitmap(mBitmap);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block e.printStackTrace(); }
		}
		
		

		final Button cropButton = (Button) findViewById(R.id.Button_crop);
		cropButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				croppedImage = cropImageView.getCroppedImage();
				crop2 = cropImageView.getCroppedImage();

				/*
				 * Pix pix_kawung = ReadFile.readBitmap(croppedImage);
				 * pix_kawung = Binarize.otsuAdaptiveThreshold(pix_kawung);
				 * croppedImage = WriteFile.writeBitmap(pix_kawung);
				 */
				
				//file.delete();
				// showDialog(SURF_PROGRESS_BAR);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				croppedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				Intent i = new Intent(Crop.this, Result.class);
				i.putExtra("bmp", byteArray);
				i.putExtra("size", 1);
				// Toast.makeText(getApplicationContext(),
				// String.valueOf(matches.size().height),
				// Toast.LENGTH_LONG).show();
				// startActivity(i);
				new Correspond().execute(0l);

			}
		});

		/*
		 * ImageView imgview = (ImageView) findViewById(R.id.surfimage);
		 * surfedbitmap = (Bitmap) getLastNonConfigurationInstance();
		 * //imgview.setImageBitmap(surfedbitmap); surflib = new SurfLib();
		 */

		/*
		 * OpenSURF mOpenCV = new OpenSURF();
		 * 
		 * 
		 * 
		 * if (!mOpenCV.setSourceImage(pixels, width, height)) {
		 * 
		 * Log.d("Opencv",
		 * "Error occurred while setting the source image pixels"); }
		 * 
		 * Ipoint ip = new Ipoint(); ip.setDx(10);
		 */

		surflib = new SurfLib();

	}
	
	public void generateNoteOnSD(String sFileName, String sBody){
	    try
	    {
	        File root = new File(Environment.getExternalStorageDirectory(), "Notes");
	        if (!root.exists()) {
	            root.mkdirs();
	        }
	        File gpxfile = new File(root, sFileName);
	        FileWriter writer = new FileWriter(gpxfile);
	        writer.append(sBody);
	        writer.flush();
	        writer.close();
	        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
	    }
	    catch(IOException e)
	    {
	         e.printStackTrace();
	         
	    }
	   } 

	@Override
	protected void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putInt(ASPECT_RATIO_X, mAspectRatioX);
		outState.putInt(ASPECT_RATIO_Y, mAspectRatioY);
	}

	
	

	private class Correspond extends AsyncTask<Long, Long[], Long[]> {
		
		String hasil = "";
		
		@Override
         protected void onPreExecute() {
             super.onPreExecute();
             // Shows Progress Bar Dialog and then call doInBackground method
             showDialog(SURF_PROGRESS_BAR);
         }
		
		@Override
		protected Long[] doInBackground(Long... id) {
			
			
			Mat image = new Mat();

			Utils.bitmapToMat(croppedImage, image);
			Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
			Imgproc.threshold(image, image, -1.0, 255.0, Imgproc.THRESH_OTSU);
			Bitmap input = Bitmap.createBitmap(croppedImage.getWidth(), croppedImage.getHeight(), croppedImage.getConfig());
			Utils.matToBitmap(image, input);
			SurfInfo info = surflib.surfify(input, false);
			Long[] match = new Long[SplashActivity.pointVector.length];
			IpPairVector matches = new IpPairVector(SplashActivity.pointVector.length);
			matches = surflib.findMatchs(info, surflib.surfify(kawung, false).surf.getIpts());
			Long[] kawung = new Long[1];
			if(matches.size()>2){
				kawung[0] = matches.size();
				Log.e("TUGTAG", "SASASO");
				return kawung;
				
			}else{
				for(int i=0;i<SplashActivity.pointVector.length;i++){
					matches = surflib.findMatchs(info, SplashActivity.pointVector[i]);
					match[i] = matches.size();
					 
				}
				Log.e("TUGTAG", "SASASI");
				return match;
			}
			
			
			
		}

		@Override
		protected void onPostExecute(Long[] result) {
			
			dismissDialog(SURF_PROGRESS_BAR);
			
			//generateNoteOn	SD("hasil.txt", hasil);
			/*Toast.makeText(getApplicationContext(),
					String.valueOf(kawung_match.size()), Toast.LENGTH_LONG)
					.show();*/
			int batik = 0;
			Long max = 1l;
			for(int i=0;i<result.length;i++){
				if (result[i] > max) {
					batik = i+1;
					max = result[i];
				}
			}
			
			
			 if(max<=1) {
					showDialog(SURF_FAILED);
					
				}else{
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					croppedImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byte[] byteArray = stream.toByteArray();
					Intent i = new Intent(Crop.this, Result.class);
					i.putExtra("bmp", byteArray);
					i.putExtra("size", batik);
					startActivity(i);
				}
			 
			
		}
	}

	SurfLib surflib;

	public Bitmap loadBitmapFromResource(int resourceId, Context ctx) {

		BitmapFactory.Options ops = new BitmapFactory.Options();

		InputStream is = ctx.getResources().openRawResource(resourceId);
		Bitmap bitmap = loadBitmapFromStream(is, ops);
		return bitmap;
	}

	public static final Bitmap loadBitmapFromStream(InputStream stream,
			BitmapFactory.Options ops) {
		Bitmap bitmap = null;

		try {

			bitmap = BitmapFactory.decodeStream(stream, null, ops);

			stream.close();

		} catch (IOException e) {

		}

		return bitmap;

	}

}