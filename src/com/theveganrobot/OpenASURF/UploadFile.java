package com.theveganrobot.OpenASURF;

import java.io.File;
import java.io.FileNotFoundException;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Environment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ugm.batikcam.R;

public class UploadFile {
	
	private String address = "http://alfiantriplek.url.ph/";
	
	
	
	public void postImage(String filePath, String nama, String kegunaan, String filosofi,final Context ctx){
        AsyncHttpClient client = new AsyncHttpClient();

        File myFile = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/all_batik", filePath);
        RequestParams params = new RequestParams();
        try {
        	params.put("nama", nama);
            params.put("kegunaan", kegunaan);
            params.put("filosofi", filosofi);
            params.put("filename", filePath);	 
            params.put("fileToUpload", myFile);
            
            
        } catch(FileNotFoundException e) {}

        client.post(address,params,new AsyncHttpResponseHandler() {
        	ProgressDialog progres;
            @Override
            public void onStart() {            	
            	progres = new ProgressDialog(ctx, AlertDialog.THEME_HOLO_LIGHT);
    			progres.setMessage("Memproses");
    			progres.getWindow().setBackgroundDrawableResource(
    		            R.color.transparent);
    			progres.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progres.dismiss();
            }

			@Override
			public void onFailure(int arg0, org.apache.http.Header[] arg1,
					byte[] arg2, Throwable arg3) {
				// TODO Auto-generated method stub
				progres.dismiss();
				
			}

			@Override
			public void onSuccess(int arg0, org.apache.http.Header[] arg1,
					byte[] arg2) {
				// TODO Auto-generated method stub
				
			}
        });
    }

}
