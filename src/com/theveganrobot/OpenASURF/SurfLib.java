package com.theveganrobot.OpenASURF;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import com.theveganrobot.OpenASURF.swig.IpPairVector;
import com.theveganrobot.OpenASURF.swig.IpointVector;
import com.theveganrobot.OpenASURF.swig.SURFjni;
import com.theveganrobot.OpenASURF.swig.surfjnimodule;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class SurfLib {
	
	public static class SurfInfo{
		public SURFjni surf;
		
		public Bitmap orignalBitmap;
		public Bitmap outputBitmap;
		public Bitmap id;
		
		public SurfInfo(SURFjni surf, Bitmap bitmap, Bitmap rid) {
			this.surf = surf;
			this.orignalBitmap = bitmap;
			this.id = rid;
		}
	}
	
	private HashMap<Integer, SurfInfo> surfmap;
	
	public SurfLib(){
		surfmap = new HashMap<Integer, SurfInfo>();
	}
	
	public SurfInfo getSurfInfo(int rid){
		return surfmap.get(rid);
	}
	
	public IpPairVector findMatchs(SurfInfo id, IpointVector  ipts){
	
		IpointVector ipts1 = id.surf.getIpts();
		IpointVector  ipts2 = ipts;
		//Ipoint a;
		//ipts1.set(1, a.s);
		IpPairVector matches = new IpPairVector();
	
		surfjnimodule.getMatches(ipts1, ipts2, matches);
		return matches;
	}

	
	public SurfInfo surfify(Bitmap rid, Context ctx){
		return surfify(rid, true);
	}

	
	public SurfInfo surfify(Bitmap rid, boolean draw){
		Bitmap bmp = rid;
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		int[] pixels = new int[width * height];
		
		
		bmp.getPixels(pixels, 0, width, 0, 0, width, height);
		
		
		SURFjni surf = new SURFjni(pixels, width, height);
		// surf.surfDetDes();
		surf.surfDetDes(false, 4, 4, 2, 0.0002f);

		
		
		SurfInfo info = new SurfInfo(surf, bmp, rid);

		return info;
		
		
	}
	

	public Bitmap loadBitmapFromResource(int resourceId, Context ctx) {

		BitmapFactory.Options ops = new BitmapFactory.Options();

		InputStream is = ctx.getResources().openRawResource(resourceId);
		Bitmap bitmap = loadBitmapFromStream(is, ops);
		return bitmap;
	}

	public static final Bitmap loadBitmapFromStream(InputStream stream,
			BitmapFactory.Options ops) {
		Bitmap bitmap = null;

		try {

			bitmap = BitmapFactory.decodeStream(stream, null, ops);

			stream.close();

		} catch (IOException e) {
			Log.e("bad file read", e.toString());
		}

		return bitmap;

	}
	
}
