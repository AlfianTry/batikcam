package com.ugm.batikcam;

import java.io.IOException;
import java.io.InputStream;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.*;

public class GetBatikUpdate {
	
	private String nama;
	private String kegunaan;
	private String filosofi;
	private String filename;
	
	
	private JSONObject mJsonObj;
	
	public GetBatikUpdate(){
		executeAsyncTask();
	}
	
	private void executeAsyncTask(){
		new LongRunningGetIO().execute();
	}

	public String getNama() {
		return nama;
	}
	
	public String getKegunaan() {
		return kegunaan;
	}
	
	public String getFilosofi() {
		return filosofi;
	}
	
	public String getFilename() {
		return filename;
	}

	
	private class LongRunningGetIO extends AsyncTask <Void, Void, String> {
		protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n>0) {
				byte[] b = new byte[4096];
				n =  in.read(b);
				if (n>0) out.append(new String(b, 0, n));
			}
			return out.toString();
		}

		@Override
		protected String doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGet = new HttpGet("http://alfiantriplek.url.ph/get.php/");
			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				text = getASCIIContentFromEntity(entity);
				Log.d("do in bg", text);
			} 
			catch (Exception e) {
				return e.getLocalizedMessage();
			}

			return text;
		}
		
		@Override
		protected void onPostExecute(String results) {
			if (results!=null) {
				try{
					mJsonObj = new JSONObject(results);
					nama = mJsonObj.getString("nama");
					kegunaan = mJsonObj.getString("kegunaan");
					filosofi = mJsonObj.getString("filosofi");
					filename = mJsonObj.getString("filename");
				}
				catch(JSONException e){
					e.getMessage();
				}
				
			}
		}

	}	
}
