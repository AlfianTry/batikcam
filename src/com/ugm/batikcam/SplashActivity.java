package com.ugm.batikcam;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import com.theveganrobot.OpenASURF.SurfLib;
import com.theveganrobot.OpenASURF.SurfLib.SurfInfo;
import com.theveganrobot.OpenASURF.swig.IpointVector;

public class SplashActivity extends Activity {

	// private final int SPLASH_DISPLAY_LENGHT = 1200;
	public static SurfInfo[] info = new SurfInfo[1];
	public static IpointVector pointV;
	public static IpointVector[] pointVector = new IpointVector[30];
	
	String getdx, getdy, ori, scale, x, y, clust, lapl = "";

	static {
		try {
			System.loadLibrary("opencv");
			System.loadLibrary("OpenSURF");
			System.loadLibrary("opencv_java");

		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
		}

	}

	SurfLib surf = new SurfLib();
	
	/** Called when the activity is first created. */
	public class AsyncTaskLoadFiles extends AsyncTask<Void, String, Void> {
		// LinearLayout linlaHeaderProgress = (LinearLayout)
		// findViewById(R.id.linlaHeaderProgress);
		/*SurfLib surf = new SurfLib();*/

		@Override
		protected void onPreExecute() {
			setContentView(R.layout.activity_splash);
			// linlaHeaderProgress.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Bitmap kawung = BitmapFactory.decodeResource(getResources(), R.drawable.kawung_cakra);
			
			
			pointVector[0] = read("kawung.dat");
			pointVector[1] = read("sidomukti.dat");
			pointVector[2] = read( "grompol.dat");
			pointVector[3] = read( "klitik.dat");
			pointVector[4] = read( "kembangmetulayar.dat");
			pointVector[5] = read( "ksatrian.dat");
			pointVector[6] = read( "lerekparang.dat");
			pointVector[7] = read( "nagasari.dat");
			pointVector[8] = read( "nitikketongkeng.dat");
			pointVector[9] = read( "nitik.dat");
			pointVector[10] = read( "parangbarong.dat");
			pointVector[11] = read( "parangbligon.dat");
			pointVector[12] = read( "parangcurigo.dat");
			pointVector[13] = read( "parangkusumo.dat");
			pointVector[14] = read( "parangtuding.dat");
			pointVector[15] = read( "sapiturang.dat");
			pointVector[16] = read( "sekarjagad.dat");
			pointVector[17] = read( "sekarpolo.dat");
			pointVector[18] = read( "semengurdo.dat");
			pointVector[19] = read( "semenkuncoro.dat");
			pointVector[20] = read( "semenromogurdo.dat");
			pointVector[21] = read( "sidoasih.dat");
			pointVector[22] = read( "slobog.dat");
			pointVector[23] = read( "tambalkanoman.dat");
			pointVector[24] = read( "tirtaterja.dat");
			pointVector[25] = read( "truntumsrikuncoro.dat");
			pointVector[26] = read( "udanliris.dat");
			pointVector[27] = read( "wahyutumuruncantel.dat");
			pointVector[28] = read( "wahyutumurun.dat");
			pointVector[29] = read( "ciptoning.dat");
			return null;
		}
		
		public void write(ArrayList<float[]> des, String filename){
			String ser = SerializeObject.objectToString(des);
			if (ser != null && !ser.equalsIgnoreCase("")) {
			    SerializeObject.WriteSettings(SplashActivity.this, ser, filename);
			} else {
			    SerializeObject.WriteSettings(SplashActivity.this, "", filename);
			}
		}
		
		public IpointVector read(String filename){
			String ser = SerializeObject.ReadSettings(SplashActivity.this, filename);
			ArrayList<float[]> des = new ArrayList<float[]>();
			if (ser != null && !ser.equalsIgnoreCase("")) {
			    Object obj = SerializeObject.stringToObject(ser);
			    // Then cast it to your object and 
			    if (obj instanceof ArrayList) {
			        // Do something
			        des = (ArrayList<float[]>)obj;
			    }
			}
			
			pointV = new IpointVector(des.size());
			for(int i = 0 ; i<des.size();i++){
				pointV.get(i).setDescriptor(des.get(i));
			}
			
			return pointV;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Intent mainIntent = new Intent(SplashActivity.this,
					MainActivity.class);
			SplashActivity.this.startActivity(mainIntent);
		}

	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_splash);
		AsyncTaskLoadFiles async = new AsyncTaskLoadFiles();
		async.execute();

	}
}