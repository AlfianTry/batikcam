package com.ugm.batikcam;

import java.io.File;
import java.util.ArrayList;

import com.batikcam.camera.CameraTestActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Formal extends Activity {

	private String[] drawerListViewItems;
	private DrawerLayout drawerLayout;
	private ListView drawerListView;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private boolean doubleBackToExitPressedOnce = false;

	private Button toCamera;

	AsyncTaskLoadFiles myAsyncTaskLoadFiles;

	public class AsyncTaskLoadFiles extends AsyncTask<Void, String, Void> {

		File targetDirector;
		ImageAdapter myTaskAdapter;

		public AsyncTaskLoadFiles(ImageAdapter adapter) {
			myTaskAdapter = adapter;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			String ExternalStorageDirectoryPath = Environment
					.getExternalStorageDirectory().getAbsolutePath();

			File folder = new File(ExternalStorageDirectoryPath + "/formal/");
			boolean success = true;
			if (!folder.exists()) {
				success = folder.mkdir();
			}
			if (success) {
				// Do something on success
			} else {
				// Do something else on failure
			}
			String targetPath = ExternalStorageDirectoryPath + "/formal/";
			targetDirector = new File(targetPath);
			myTaskAdapter.clear();

		}

		@Override
		protected Void doInBackground(Void... params) {

			File[] files = targetDirector.listFiles();
			for (File file : files) {
				publishProgress(file.getAbsolutePath());
				if (isCancelled())
					break;
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			myTaskAdapter.add(values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			myTaskAdapter.notifyDataSetChanged();

		}

	}

	public class ImageAdapter extends BaseAdapter {

		private Context mContext;
		ArrayList<String> itemList = new ArrayList<String>();

		public ImageAdapter(Context c) {
			mContext = c;
		}

		void add(String path) {
			itemList.add(path);
		}

		void clear() {
			itemList.clear();
		}

		void remove(int index) {
			itemList.remove(index);
		}

		@Override
		public int getCount() {
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			if (convertView == null) { // if it's not recycled, initialize some
										// attributes
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(196, 196));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}

			Bitmap bm = decodeSampledBitmapFromUri(itemList.get(position), 196,
					196);

			imageView.setImageBitmap(bm);
			return imageView;
		}

		public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
				int reqHeight) {

			Bitmap bm = null;
			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bm = BitmapFactory.decodeFile(path, options);

			return bm;
		}

		public int calculateInSampleSize(

		BitmapFactory.Options options, int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				if (width > height) {
					inSampleSize = Math.round((float) height
							/ (float) reqHeight);
				} else {
					inSampleSize = Math.round((float) width / (float) reqWidth);
				}

			}

			return inSampleSize;
		}

	}

	ImageAdapter myImageAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		BitmapDrawable background = new BitmapDrawable(
				BitmapFactory
						.decodeResource(getResources(), R.drawable.formal));
		background.setTileModeX(android.graphics.Shader.TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(background);
		// get list items from .xml
		drawerListViewItems = getResources().getStringArray(R.array.items);
		// get ListView defined in activity_main.xml
		drawerListView = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		drawerListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_listview_item, drawerListViewItems));

		// 2. App Icon
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// 2.1 create ActionBarDrawerToggle
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		drawerLayout, /* DrawerLayout ` */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		);

		// 2.2 Set actionBarDrawerToggle as the DrawerListener
		drawerLayout.setDrawerListener(actionBarDrawerToggle);

		// 2.3 enable and show "up" arrow
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// just styling option
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		drawerListView.setOnItemClickListener(new DrawerItemClickListener());

		// start - drawing grid
		final GridView gridview = (GridView) findViewById(R.id.content_grid);
		myImageAdapter = new ImageAdapter(this);
		gridview.setAdapter(myImageAdapter);

		gridview.setOnItemClickListener(myOnItemClickListener);
		// end - drawing grid

		toCamera = (Button) findViewById(R.id.tocamera);
		toCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent nextScreen = new Intent(getApplicationContext(),
						CameraTestActivity.class);
				startActivity(nextScreen);
			}
			/*
			 * if (client.checkSession()) { if (client.checkActivation()) {
			 * Intent nextScreen = new Intent(getApplicationContext(),
			 * CameraTestActivity.class); startActivity(nextScreen); } else {
			 * //loged, but not activated Intent nextScreen = new
			 * Intent(getApplicationContext(), VerificationActivity.class);
			 * startActivity(nextScreen); }
			 * 
			 * 
			 * } else { //not loged in, to account Intent nextScreen = new
			 * Intent(getApplicationContext(), LoginActivity.class);
			 * startActivity(nextScreen); } }
			 */
		});
		myAsyncTaskLoadFiles = new AsyncTaskLoadFiles(myImageAdapter);
		myAsyncTaskLoadFiles.execute();
	}

	OnItemClickListener myOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			String path = (String) parent.getItemAtPosition(position);

			Intent i = new Intent(Formal.this, SingleActivity.class);
			i.putExtra("imgpath", path);

			startActivity(i);

		}
	};

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		/*
		 * switch (item.getItemId()) { case R.id.action_sync: Log.d("MYID",
		 * "JUST CLICK SYNC!"); // client.refreshToken(); client.sync(); break;
		 * case R.id.action_sync_inactive: Toast t =
		 * Toast.makeText(getApplicationContext(), "You are not Loged In",
		 * Toast.LENGTH_SHORT); t.show(); break; }
		 */

		// call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns
		// true
		// then it has handled the app icon touch event

		if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		/*
		 * if (client.checkSession() == true) {
		 * menu.findItem(R.id.action_sync_inactive).setVisible(false); } else {
		 * menu.findItem(R.id.action_sync).setVisible(false); }
		 */
		return true;
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// Toast.makeText(MainActivity.this, ((TextView)view).getText(),
			// Toast.LENGTH_LONG).show();
			String chosen = (String) ((TextView) view).getText();

			if (chosen.equals("All Batik")) {
				Intent i = new Intent(Formal.this, MainActivity.class);
				startActivity(i);
			} else if (chosen.equals("Wedding")) {
				Intent i = new Intent(Formal.this, Wedding.class);
				startActivity(i);
			} else if (chosen.equals("History")) {
				Intent i = new Intent(Formal.this, History.class);
				startActivity(i);
			} else if (chosen.equals("Local Suit")) {
				Intent i = new Intent(Formal.this, Local_Suit.class);
				startActivity(i);
			} else if (chosen.equals("About")) {
				Intent i = new Intent(Formal.this, AboutActivity.class);
				startActivity(i);
			} else if (chosen.equals("Daily")) {
				Intent i = new Intent(Formal.this, Daily.class);
				startActivity(i);
			} else if (chosen.equals("Funeral")) {
				Intent i = new Intent(Formal.this, Funeral.class);
				startActivity(i);
			} else if (chosen.equals("Formal")) {
				/*Intent i = new Intent(Formal.this, Formal.class);
				startActivity(i);*/
			}

			drawerLayout.closeDrawer(drawerListView);

		}
	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			// in
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click BACK again to exit",
				Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);
	}
}