package com.ugm.batikcam;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

public class AboutActivity extends Activity {

	Button btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final GetBatikUpdate get = new GetBatikUpdate();
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		btn = (Button) findViewById(R.id.button1);
		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//Toast.makeText(getBaseContext(), get.getFilename(), Toast.LENGTH_SHORT).show();
				AsyncHttpClient client = new AsyncHttpClient();
				client.get(
						"http://alfiantriplek.url.ph/uploads/"+get.getFilename(),
						new FileAsyncHttpResponseHandler(AboutActivity.this) {

							ProgressDialog progres;

							@Override
							public void onStart() {
								progres = new ProgressDialog(AboutActivity.this,
										AlertDialog.THEME_HOLO_LIGHT);
								progres.setMessage("Memproses");
								progres.getWindow()
										.setBackgroundDrawableResource(
												R.color.transparent);
								progres.show();
							}

							@Override
							public void onFinish() {
								super.onFinish();
								progres.dismiss();
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									Throwable arg2, File arg3) {
								// TODO Auto-generated method stub
								Toast.makeText(getBaseContext(), "NETWORK CONNECTION ERROR", Toast.LENGTH_LONG).show();

							}

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									File arg2) {
								// TODO Auto-generated method stub
								Bitmap myBitmap = BitmapFactory.decodeFile(arg2
										.getAbsolutePath());
								saveImageToExternalStorage(myBitmap, "batiks");
								progres.dismiss();

							}

						});

			}
		});
	}

	public void saveImageToExternalStorage(Bitmap image, String nama_batik) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/all_batik";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyD_HHmmss")
					.format(new Date());
			String imageFileName = nama_batik + "_" + timeStamp + ".jpg";
			String str = imageFileName;
			File file = new File(fullPath, imageFileName);
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			
		}
	}

}
