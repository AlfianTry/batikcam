package com.ugm.batikcam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.TextView;
import android.widget.Toast;

import com.batikcam.camera.CameraTestActivity;
import com.edmodo.cropper.R.color;
import com.theveganrobot.OpenASURF.SurfLib;

public class MainActivity extends Activity {

	private String[] drawerListViewItems;
	private DrawerLayout drawerLayout;
	private ListView drawerListView;
	private ActionBarDrawerToggle actionBarDrawerToggle;
	private boolean doubleBackToExitPressedOnce = false;
	// Bitmap kawung;

	private Button toCamera;
	private String search = "";
	AsyncTaskLoadFiles myAsyncTaskLoadFiles;

	public class AsyncTaskLoadFiles extends AsyncTask<Void, String, Void> {

		File targetDirector1, targetDirector2, targetDirector3,
				targetDirector4, targetDirector5, targetDirector6;
		ImageAdapter myTaskAdapter;
		LinearLayout linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);

		public AsyncTaskLoadFiles(ImageAdapter adapter) {

			myTaskAdapter = adapter;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			linlaHeaderProgress.setVisibility(View.VISIBLE);
			String ExternalStorageDirectoryPath = Environment
					.getExternalStorageDirectory().getAbsolutePath();
			String targetPath1 = ExternalStorageDirectoryPath + "/wedding/";
			String targetPath2 = ExternalStorageDirectoryPath + "/daily/";
			String targetPath3 = ExternalStorageDirectoryPath + "/local/";
			String targetPath4 = ExternalStorageDirectoryPath + "/funeral/";
			String targetPath5 = ExternalStorageDirectoryPath + "/formal/";
			String targetPath6 = ExternalStorageDirectoryPath + "/all_batik/";

			targetDirector1 = new File(targetPath1);
			targetDirector2 = new File(targetPath2);
			targetDirector3 = new File(targetPath3);
			targetDirector4 = new File(targetPath4);
			targetDirector5 = new File(targetPath5);
			targetDirector6 = new File(targetPath6);
			myTaskAdapter.clear();
		}

		@Override
		protected Void doInBackground(Void... params) {

			File[] files1 = targetDirector1.listFiles();
			File[] files2 = targetDirector2.listFiles();
			File[] files3 = targetDirector3.listFiles();
			File[] files4 = targetDirector4.listFiles();
			File[] files5 = targetDirector5.listFiles();
			File[] files6 = targetDirector6.listFiles();

			for (File file : files1) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}
			for (File file : files2) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}
			for (File file : files3) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}
			for (File file : files4) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}
			for (File file : files5) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}

			for (File file : files6) {
				if (file.getName().contains(search.toLowerCase())) {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				} else if (search == "") {
					publishProgress(file.getAbsolutePath());
					Log.d("TIK-TAK", file.getName());
					if (isCancelled())
						break;
				}
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			myTaskAdapter.add(values[0]);
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			linlaHeaderProgress.setVisibility(View.GONE);
			myTaskAdapter.notifyDataSetChanged();

		}

	}

	public class ImageAdapter extends BaseAdapter {

		private Context mContext;
		ArrayList<String> itemList = new ArrayList<String>();

		public ImageAdapter(Context c) {
			mContext = c;
		}

		void add(String path) {
			itemList.add(path);
		}

		void clear() {
			itemList.clear();
		}

		void remove(int index) {
			itemList.remove(index);
		}

		@Override
		public int getCount() {
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			if (convertView == null) { // if it's not recycled, initialize some
										// attributes
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(196, 196));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(8, 8, 8, 8);
			} else {
				imageView = (ImageView) convertView;
			}

			Bitmap bm = decodeSampledBitmapFromUri(itemList.get(position), 196,
					196);

			imageView.setImageBitmap(bm);
			return imageView;
		}

		public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
				int reqHeight) {

			Bitmap bm = null;
			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bm = BitmapFactory.decodeFile(path, options);

			return bm;
		}

		public int calculateInSampleSize(

		BitmapFactory.Options options, int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				if (width > height) {
					inSampleSize = Math.round((float) height
							/ (float) reqHeight);
				} else {
					inSampleSize = Math.round((float) width / (float) reqWidth);
				}

			}

			return inSampleSize;
		}

	}

	ImageAdapter myImageAdapter;
	SurfLib surflib;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		BitmapDrawable background = new BitmapDrawable(
				BitmapFactory
						.decodeResource(getResources(), R.drawable.headers));
		background.setTileModeX(android.graphics.Shader.TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(background);

		// get list items from .xml
		drawerListViewItems = getResources().getStringArray(R.array.items);
		// get ListView defined in activity_main.xml
		drawerListView = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		drawerListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_listview_item, drawerListViewItems));

		// 2. App Icon
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// 2.1 create ActionBarDrawerToggle
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		drawerLayout, /* DrawerLayout ` */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		);

		// 2.2 Set actionBarDrawerToggle as the DrawerListener
		drawerLayout.setDrawerListener(actionBarDrawerToggle);

		// 2.3 enable and show "up" arrow
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// just styling option
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		drawerListView.setOnItemClickListener(new DrawerItemClickListener());

		// start - drawing grid
		final GridView gridview = (GridView) findViewById(R.id.content_grid);
		myImageAdapter = new ImageAdapter(this);
		gridview.setAdapter(myImageAdapter);

		gridview.setOnItemClickListener(myOnItemClickListener);
		// end - drawing grid

		String ExternalStorageDirectoryPath = Environment
				.getExternalStorageDirectory().getAbsolutePath();

		File wedding = new File(ExternalStorageDirectoryPath + "/wedding");
		File funeral = new File(ExternalStorageDirectoryPath + "/funeral");
		File formal = new File(ExternalStorageDirectoryPath + "/formal");
		File localsuit = new File(ExternalStorageDirectoryPath + "/local");
		File daily = new File(ExternalStorageDirectoryPath + "/daily");
		File all = new File(ExternalStorageDirectoryPath + "/all_batik");

		boolean defaultFoldersuccess = true;
		if (!wedding.exists()) {
			defaultFoldersuccess = wedding.mkdir();
		}
		if (!funeral.exists()) {
			defaultFoldersuccess = funeral.mkdir();
		}
		if (!formal.exists()) {
			defaultFoldersuccess = formal.mkdir();
		}
		if (!localsuit.exists()) {
			defaultFoldersuccess = localsuit.mkdir();
		}
		if (!daily.exists()) {
			defaultFoldersuccess = daily.mkdir();
		}
		if (!all.exists()) {
			defaultFoldersuccess = all.mkdir();
		}
		/*
		 * if (!defaultFolder.exists()) { defaultFoldersuccess =
		 * defaultFolder.mkdir(); }
		 */
		if (defaultFoldersuccess) {
			// Do something on success
			Bitmap[] bm = new Bitmap[31];
			bm[0] = BitmapFactory.decodeResource(getResources(),
					R.drawable.wahyu_tumurun_cantel);
			bm[1] = BitmapFactory.decodeResource(getResources(),
					R.drawable.wahyu_tumurun);
			bm[2] = BitmapFactory.decodeResource(getResources(),
					R.drawable.ciptoning);
			bm[3] = BitmapFactory.decodeResource(getResources(),
					R.drawable.udan_liris);
			bm[4] = BitmapFactory.decodeResource(getResources(),
					R.drawable.grompol);
			bm[5] = BitmapFactory.decodeResource(getResources(),
					R.drawable.truntum_sri_kuncoro);
			bm[6] = BitmapFactory.decodeResource(getResources(),
					R.drawable.tirta_teja);
			bm[7] = BitmapFactory.decodeResource(getResources(),
					R.drawable.kembang_metu_layar);
			bm[8] = BitmapFactory.decodeResource(getResources(),
					R.drawable.kawungs);
			bm[9] = BitmapFactory.decodeResource(getResources(),
					R.drawable.klitik);
			bm[10] = BitmapFactory.decodeResource(getResources(),
					R.drawable.ksatrian);
			bm[11] = BitmapFactory.decodeResource(getResources(),
					R.drawable.tambal_kanoman);
			bm[12] = BitmapFactory.decodeResource(getResources(),
					R.drawable.sido_asih);
			bm[13] = BitmapFactory.decodeResource(getResources(),
					R.drawable.sido_mukti_luhur);
			bm[14] = BitmapFactory.decodeResource(getResources(),
					R.drawable.sekar_jagad);
			bm[15] = BitmapFactory.decodeResource(getResources(),
					R.drawable.sekar_polo);
			bm[16] = BitmapFactory.decodeResource(getResources(),
					R.drawable.semen_gurdo);
			bm[17] = BitmapFactory.decodeResource(getResources(),
					R.drawable.semen_kuncoro);
			bm[18] = BitmapFactory.decodeResource(getResources(),
					R.drawable.semen_romo_gurdo);
			bm[19] = BitmapFactory.decodeResource(getResources(),
					R.drawable.lerek_parang);
			bm[20] = BitmapFactory.decodeResource(getResources(),
					R.drawable.lung_kangkung);
			bm[21] = BitmapFactory.decodeResource(getResources(),
					R.drawable.nagasari);
			bm[22] = BitmapFactory.decodeResource(getResources(),
					R.drawable.nitik);
			bm[23] = BitmapFactory.decodeResource(getResources(),
					R.drawable.nitik_ketongkeng);
			bm[24] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_barong);
			bm[25] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_bligon);
			bm[26] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_curigo);
			bm[27] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_grompol);
			bm[28] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_kusumo);
			bm[29] = BitmapFactory.decodeResource(getResources(),
					R.drawable.parang_tuding);
			bm[30] = BitmapFactory.decodeResource(getResources(),
					R.drawable.sapit_urang);

			String extStorageDirectory = Environment
					.getExternalStorageDirectory().toString();
			try {

				File[] file = new File[bm.length];
				file[0] = new File(extStorageDirectory
						+ "/wedding/wahyu_tumurun_cantel.jpg");
				file[1] = new File(extStorageDirectory
						+ "/local/wahyu_tumurun.jpg");
				file[2] = new File(extStorageDirectory
						+ "/formal/ciptoning.jpg");
				file[3] = new File(extStorageDirectory
						+ "/daily/udan_liris.jpg");
				file[4] = new File(extStorageDirectory + "/wedding/grompol.jpg");
				file[5] = new File(extStorageDirectory
						+ "/wedding/truntum_sri_kuncoro.jpg");
				file[6] = new File(extStorageDirectory
						+ "/daily/tirta_teja.jpg");
				file[7] = new File(extStorageDirectory
						+ "/daily/kembang_temu_latar.jpg");
				file[8] = new File(extStorageDirectory
						+ "/daily/kawung_picis.jpg");
				file[9] = new File(extStorageDirectory + "/formal/klitik.jpg");
				file[10] = new File(extStorageDirectory
						+ "/wedding/ksatrian.jpg");
				file[11] = new File(extStorageDirectory
						+ "/daily/tambal_kanoman.jpg");
				file[12] = new File(extStorageDirectory
						+ "/formal/sido asih.jpg");
				file[13] = new File(extStorageDirectory
						+ "/daily/sido mukti.jpg");
				file[14] = new File(extStorageDirectory
						+ "/local/sekar jagad.jpg");
				file[15] = new File(extStorageDirectory
						+ "/wedding/sekar polo.jpg");
				file[16] = new File(extStorageDirectory
						+ "/funeral/semen gurdo.jpg");
				file[17] = new File(extStorageDirectory
						+ "/funeral/semen kuncoro.jpg");
				file[18] = new File(extStorageDirectory
						+ "/funeral/semen romo gurdo.jpg");
				file[19] = new File(extStorageDirectory
						+ "/funeral/lerek parang.jpg");
				file[20] = new File(extStorageDirectory
						+ "/wedding/lung kangkung.jpg");
				file[21] = new File(extStorageDirectory
						+ "/local/nagasari.jpg");
				file[22] = new File(extStorageDirectory
						+ "/local/nitik.jpg");
				file[23] = new File(extStorageDirectory
						+ "/local/nitik ketongkeng.jpg");
				file[24] = new File(extStorageDirectory
						+ "/daily/parang barong.jpg");
				file[25] = new File(extStorageDirectory
						+ "/formal/parang bligon.jpg");
				file[26] = new File(extStorageDirectory
						+ "/formal/parang curigo.jpg");
				file[27] = new File(extStorageDirectory
						+ "/daily/parang grompol.jpg");
				file[28] = new File(extStorageDirectory
						+ "/daily/parang kusumo.jpg");
				file[29] = new File(extStorageDirectory
						+ "/wedding/parang tuding.jpg");
				file[30] = new File(extStorageDirectory
						+ "/daily/sapit urang.jpg");

				FileOutputStream[] outStream = new FileOutputStream[bm.length];
				for (int i = 0; i < file.length; i++) {
					if (!file[i].exists()) {
						outStream[i] = new FileOutputStream(file[i]);
						bm[i].compress(Bitmap.CompressFormat.JPEG, 100,
								outStream[i]);
						outStream[i].flush();
						outStream[i].close();
					}
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// Do something else on failure
		}

		toCamera = (Button) findViewById(R.id.tocamera);
		toCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent nextScreen = new Intent(getApplicationContext(),
						CameraTestActivity.class);
				startActivity(nextScreen);
			}
			/*
			 * if (client.checkSession()) { if (client.checkActivation()) {
			 * Intent nextScreen = new Intent(getApplicationContext(),
			 * CameraTestActivity.class); startActivity(nextScreen); } else {
			 * //loged, but not activated Intent nextScreen = new
			 * Intent(getApplicationContext(), VerificationActivity.class);
			 * startActivity(nextScreen); }
			 * 
			 * 
			 * } else { //not loged in, to account Intent nextScreen = new
			 * Intent(getApplicationContext(), LoginActivity.class);
			 * startActivity(nextScreen); } }
			 */
		});

		myAsyncTaskLoadFiles = new AsyncTaskLoadFiles(myImageAdapter);
		myAsyncTaskLoadFiles.execute();

	}

	OnItemClickListener myOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			String path = (String) parent.getItemAtPosition(position);

			Intent i = new Intent(MainActivity.this, SingleActivity.class);
			i.putExtra("imgpath", path);
			Log.d("TIGTAG", path);
			startActivity(i);

		}
	};

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		setSearchTextColour(searchView);
		searchView.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				// TODO Auto-generated method stub
				if (search != "") {
					search = "";
					AsyncTaskLoadFiles async = new AsyncTaskLoadFiles(
							myImageAdapter);
					async.execute();
					return true;
				}
				return false;
			}
		});
		try {
			Field searchField = SearchView.class
					.getDeclaredField("mCloseButton");
			searchField.setAccessible(true);
			ImageView closeBtn = (ImageView) searchField.get(searchView);
			closeBtn.setImageResource(R.drawable.close);

		} catch (NoSuchFieldException e) {
			Log.e("SearchView", e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Log.e("SearchView", e.getMessage(), e);
		}
		return super.onCreateOptionsMenu(menu);
	}

	private void setSearchTextColour(final SearchView searchView) {
		int searchPlateId = searchView.getContext().getResources()
				.getIdentifier("android:id/search_src_text", null, null);
		final EditText searchPlate = (EditText) searchView
				.findViewById(searchPlateId);
		searchPlate.setTextColor(Color.WHITE);
		// searchPlate.setBackgroundResource(R.drawable.texfield_searchview_holo_light);
		// searchPlate.setBackgroundResource(R.drawable.edit_text_holo_light);
		searchPlate.setBackgroundColor(color.black_translucent);
		searchPlate.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		searchPlate.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					Toast.makeText(getBaseContext(), searchPlate.getText(),
							Toast.LENGTH_SHORT).show();
					search = searchPlate.getText().toString();
					AsyncTaskLoadFiles AsyncTask = new AsyncTaskLoadFiles(
							myImageAdapter);
					AsyncTask.execute();
					searchView.clearFocus();
					return true;
				}
				return false;
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		/*
		 * switch (item.getItemId()) { case R.id.action_sync: Log.d("MYID",
		 * "JUST CLICK SYNC!"); // client.refreshToken(); client.sync(); break;
		 * case R.id.action_sync_inactive: Toast t =
		 * Toast.makeText(getApplicationContext(), "You are not Loged In",
		 * Toast.LENGTH_SHORT); t.show(); break; }
		 */

		// call ActionBarDrawerToggle.onOptionsItemSelected(), if it returns
		// true
		// then it has handled the app icon touch event
		switch (item.getItemId()) {
		case R.id.action_search:
			
		case R.id.action_settings:

		default:
			if (actionBarDrawerToggle.onOptionsItemSelected(item)) {

				return true;

			}

			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
		SearchView mSearchView = (SearchView) searchViewMenuItem
				.getActionView();
		int searchImgId = getResources().getIdentifier(
				"android:id/search_button", null, null);
		ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
		v.setImageResource(R.drawable.search);
		// mSearchView.setOnQueryTextListener(this);
		super.onPrepareOptionsMenu(menu);
		/*
		 * if (client.checkSession() == true) {
		 * menu.findItem(R.id.action_sync_inactive).setVisible(false); } else {
		 * menu.findItem(R.id.action_sync).setVisible(false); }
		 */
		return true;
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// Toast.makeText(MainActivity.this, ((TextView)view).getText(),
			// Toast.LENGTH_LONG).show();
			String chosen = (String) ((TextView) view).getText();

			if (chosen.equals("All Batik")) {

			} else if (chosen.equals("Wedding")) {
				Intent i = new Intent(MainActivity.this, Wedding.class);
				startActivity(i);
			} else if (chosen.equals("History")) {
				Intent i = new Intent(MainActivity.this, History.class);
				startActivity(i);
			} else if (chosen.equals("Local Suit")) {
				Intent i = new Intent(MainActivity.this, Local_Suit.class);
				startActivity(i);
			} else if (chosen.equals("About")) {
				Intent i = new Intent(MainActivity.this, AboutActivity.class);
				startActivity(i);
			} else if (chosen.equals("Daily")) {
				Intent i = new Intent(MainActivity.this, Daily.class);
				startActivity(i);
			} else if (chosen.equals("Funeral")) {
				Intent i = new Intent(MainActivity.this, Funeral.class);
				startActivity(i);
			} else if (chosen.equals("Formal")) {
				Intent i = new Intent(MainActivity.this, Formal.class);
				startActivity(i);
			}

			drawerLayout.closeDrawer(drawerListView);

		}
	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			// in
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click BACK again to exit",
				Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);

	}
	

}