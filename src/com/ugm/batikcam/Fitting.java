package com.ugm.batikcam;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.YuvImage;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.ugm.batikcam.MainActivity;
import com.ugm.batikcam.R;

/* Import ZBar Class files */

public class Fitting extends Activity {

	public int status = 0;
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	protected static final String TAG = null;
	protected static final Throwable e = null;
	Rect rect = new Rect(0, 0, 480, 800);
	Bitmap gambar = null;
	TextView scanText;
	Button scanButton;
	Bitmap bitmap;

	FrameLayout frml;
	DrawingView drawingView;

	private boolean previewing = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_camera);
		frml = (FrameLayout) findViewById(R.id.cameraPreview);
		drawingView = new DrawingView(this);
		LayoutParams layoutParamsDrawing = new LayoutParams(
				LayoutParams.WRAP_CONTENT, 685);
		this.addContentView(drawingView, layoutParamsDrawing);

		

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();
		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);
		scanButton = (Button) findViewById(R.id.ScanButton);
		getActionBar().hide();

	}

	@Override
	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	public void releaseCamera() {
		if (mCamera != null) {
			mPreview.getHolder().removeCallback(mPreview);
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		@Override
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {

		@Override
		public void onPreviewFrame(final byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();

			final Size size = parameters.getPreviewSize();


			scanButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					//mPreview.takePicture();
					scanButton.setClickable(false);
					

						previewing = false;
						mCamera.setPreviewCallback(null);

						final YuvImage image = new YuvImage(data,
								ImageFormat.NV21, size.width, size.height, null);
						ByteArrayOutputStream os = new ByteArrayOutputStream(
								data.length);
						if (!image.compressToJpeg(new Rect(0, 0, size.width,
								size.height), 100, os)) {
							return;
						}
						byte[] tmp = os.toByteArray();

						Bitmap bmp = BitmapFactory.decodeByteArray(tmp, 0,
								tmp.length);
						Matrix matrix = new Matrix();
						matrix.postRotate(90);
						bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
								bmp.getHeight(), matrix, true);
						onPause();
						
						Bitmap bottomImage = bmp; // blue
						matrix.postRotate(90);
						Bitmap bitmaps = Bitmap.createBitmap(bottomImage.getWidth(),
								bottomImage.getHeight(), Bitmap.Config.ARGB_8888);
						Canvas c = new Canvas(bitmaps);
						Resources res = getResources();

						Bitmap topImage = drawingView.get(); // green
						Drawable drawable1 = new BitmapDrawable(bottomImage);
						Drawable drawable2 = new BitmapDrawable(topImage);

						drawable1.setBounds(0, 0, bottomImage.getWidth(),
								bottomImage.getHeight());
						drawable2.setBounds(0, 0, topImage.getWidth(),
								topImage.getHeight());
						drawable1.draw(c);
						drawable2.draw(c);
						
						saveImageToExternalStorage(bitmaps, "fitting");
						
						Intent nextScreen = new Intent(getApplicationContext(),
								MainActivity.class);
						startActivity(nextScreen);

					
				}

			});

		}
	};

	public void saveImageToExternalStorage(Bitmap image, String a) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/fitting";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());
			String imageFileName = a;
			File file = new File(fullPath, imageFileName + timeStamp + ".jpg");
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			Log.e("saveToExternalStorage()", e.getMessage());
		}
	}

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent toCamera = new Intent(Fitting.this,
				MainActivity.class);
		startActivity(toCamera);
	}

	private class DrawingView extends View {
		public float mPosX;
	    public float mPosY;
//	    BitmapDrawable mImage = new BitmapDrawable(getResources(),
//				bitmap);
	    
	    Drawable mImage;
	    private static final int INVALID_POINTER_ID = -1;
	    private float mLastTouchX;
	    private float mLastTouchY;
	    private int mActivePointerId = INVALID_POINTER_ID;

	    private ScaleGestureDetector mScaleDetector;
	    private float mScaleFactor = 1.f;

		Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);

		public DrawingView(Context context) {
			
	        this(context, null, 0);
	        Bundle extras = getIntent().getExtras();
			byte[] byteArray = extras.getByteArray("bmp");
			Bitmap batiks = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
	        /*Bitmap batiks = BitmapFactory.decodeResource(getResources(),
					R.drawable.kawungz);*/
			Bitmap batik = Bitmap.createScaledBitmap(batiks, batiks.getWidth() / 2,
					batiks.getHeight() / 2, false);
			BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),
					batik);
			bitmapDrawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
			bitmapDrawable.setBounds(rect);
			Resources res = getResources();
			Bitmap mask = BitmapFactory.decodeResource(res, R.drawable.klambi);
			gambar = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(),
					Bitmap.Config.ARGB_8888);
			Canvas cvs = new Canvas(gambar);
			bitmapDrawable.draw(cvs);

			BitmapFactory.Options options = new BitmapFactory.Options();
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				// Starting with Honeycomb, we can load the bitmap as mutable.
				options.inMutable = true;
			}
			// We could also use ARGB_4444, but not RGB_565 (we need an alpha
			// layer).
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;

			Bitmap sources = gambar;
			Bitmap source = Bitmap.createScaledBitmap(sources, mask.getWidth(),
					mask.getHeight(), false);

			if (source.isMutable()) {
				bitmap = source;
			} else {
				bitmap = source.copy(Bitmap.Config.ARGB_8888, true);
				source.recycle();
			}
			// The bitmap is opaque, we need to enable alpha compositing.

			bitmap.setHasAlpha(true);
			Canvas canvas = new Canvas(bitmap);

			Paint paint = new Paint();
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
			canvas.drawBitmap(mask, 0, 0, paint);
			// We do not need the mask bitmap anymore.
			mask.recycle();
	        mImage = getResources().getDrawable(R.drawable.kawungz);
	        //mImage.setBounds(0, 0, mImage.getIntrinsicWidth(), mImage.getIntrinsicHeight());
	        this.setDrawingCacheEnabled(true);
	    }

	    public DrawingView(Context context, AttributeSet attrs) {
	        this(context, attrs, 0);
	        this.setDrawingCacheEnabled(true);
	    }

	    public DrawingView(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
	        this.setDrawingCacheEnabled(true);
	    }
	    
		@Override
		public void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			Bitmap png;
	        canvas.save();
	        Log.d("DEBUG", "X: "+mPosX+" Y: "+mPosY);
	        canvas.translate(mPosX, mPosY);
	        canvas.scale(mScaleFactor, mScaleFactor);
	        mImage.draw(canvas);
	        canvas.drawBitmap(bitmap, mPosX, mPosY, null);
	        canvas.restore();
	        
		}
		
		public Bitmap get(){
			   return this.getDrawingCache();
			}

		@Override
		public boolean onTouchEvent(MotionEvent ev) {
			mScaleDetector.onTouchEvent(ev);

	        final int action = ev.getAction();
	        switch (action & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN: {
	            final float x = ev.getX();
	            final float y = ev.getY();
	            mLastTouchX = x;
	            mLastTouchY = y;
	            mActivePointerId = ev.getPointerId(0);
	            break;
	        }

	        case MotionEvent.ACTION_MOVE: {
	            final int pointerIndex = ev.findPointerIndex(mActivePointerId);
	            final float x = ev.getX(pointerIndex);
	            final float y = ev.getY(pointerIndex);

	            // Only move if the ScaleGestureDetector isn't processing a gesture.
	            if (!mScaleDetector.isInProgress()) {
	                final float dx = x - mLastTouchX;
	                final float dy = y - mLastTouchY;

	                mPosX += dx;
	                mPosY += dy;

	                invalidate();
	            }

	            mLastTouchX = x;
	            mLastTouchY = y;

	            break;
	        }

	        case MotionEvent.ACTION_UP: {
	            mActivePointerId = INVALID_POINTER_ID;
	            break;
	        }

	        case MotionEvent.ACTION_CANCEL: {
	            mActivePointerId = INVALID_POINTER_ID;
	            break;
	        }

	        case MotionEvent.ACTION_POINTER_UP: {
	            final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) 
	                    >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
	            final int pointerId = ev.getPointerId(pointerIndex);
	            if (pointerId == mActivePointerId) {
	                // This was our active pointer going up. Choose a new
	                // active pointer and adjust accordingly.
	                final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
	                mLastTouchX = ev.getX(newPointerIndex);
	                mLastTouchY = ev.getY(newPointerIndex);
	                mActivePointerId = ev.getPointerId(newPointerIndex);
	            }
	            break;
	        }
	        }

	        return true;
		}
		
		private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
	        @Override
	        public boolean onScale(ScaleGestureDetector detector) {
	            mScaleFactor *= detector.getScaleFactor();

	            // Don't let the object get too small or too large.
	            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
	            
	            invalidate();
	            return true;
	        }
	    }
	}
	
	class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	    private SurfaceHolder mHolder;
	    private Camera mCamera;
	    private PreviewCallback previewCallback;
	    private AutoFocusCallback autoFocusCallback;

	    public CameraPreview(Context context, Camera camera,
	                         PreviewCallback previewCb,
	                         AutoFocusCallback autoFocusCb) {
	        super(context);
	        mCamera = camera;
	        previewCallback = previewCb;
	        autoFocusCallback = autoFocusCb;

	        /* 
	         * Set camera to continuous focus if supported, otherwise use
	         * software auto-focus. Only works for API level >=9.
	         */
	        /*
	        Camera.Parameters parameters = camera.getParameters();
	        for (String f : parameters.getSupportedFocusModes()) {
	            if (f == Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
	                mCamera.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
	                autoFocusCallback = null;
	                break;
	            }
	        }
	        */

	        // Install a SurfaceHolder.Callback so we get notified when the
	        // underlying surface is created and destroyed.
	        mHolder = getHolder();
	        mHolder.addCallback(this);

	        // deprecated setting, but required on Android versions prior to 3.0
	        /*mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);*/
	    }

	    /*public void surfaceCreated(SurfaceHolder holder) {
	        // The Surface has been created, now tell the camera where to draw the preview.
	        try {
	            mCamera.setPreviewDisplay(holder);
	        } catch (IOException e) {
	            Log.d("DBG", "Error setting camera preview: " + e.getMessage());
	        }
	    }*/

	    @Override
		public void surfaceDestroyed(SurfaceHolder holder) {
	        // Camera preview released in activity
	    	
	    }

	    @Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	        /*
	         * If your preview can change or rotate, take care of those events here.
	         * Make sure to stop the preview before resizing or reformatting it.
	         */
	        if (mHolder.getSurface() == null){
	          // preview surface does not exist
	          return;
	        }

	        // stop preview before making changes
	        try {
	            mCamera.stopPreview();
	        } catch (Exception e){
	          // ignore: tried to stop a non-existent preview
	        }

	        try {
	            // Hard code camera surface rotation 90 degs to match Activity view in portrait
	            mCamera.setDisplayOrientation(90);

	            mCamera.setPreviewDisplay(mHolder);
	            mCamera.setPreviewCallback(previewCallback);
	            mCamera.startPreview();
	            mCamera.autoFocus(autoFocusCallback);
	        } catch (Exception e){
	            Log.d("DBG", "Error starting camera preview: " + e.getMessage());
	        }
	    }

		@Override
		public void surfaceCreated(SurfaceHolder arg0) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			if(event.getAction() == MotionEvent.ACTION_DOWN){
				//Log.d("HALO", "EEK");
			}
			return super.onTouchEvent(event);
		}
		
	}
	
}