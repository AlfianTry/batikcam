package com.ugm.batikcam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Result extends Activity{
	
	ImageView img;
	Button btn;
	public Button save;
	public Button readmore;
	public Button fitting,delete;
	Bitmap bmp;
	String nama_batik;
	TextView nama_motif;
	JustifiedTextView deskripsi;
	int match;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.batik_result);
		img = (ImageView) findViewById(R.id.batik);
		save = (Button) findViewById(R.id.button1);
		readmore = (Button) findViewById(R.id.button2);
		fitting = (Button) findViewById(R.id.button3);
		delete = (Button) findViewById(R.id.button4);
		nama_motif = (TextView) findViewById(R.id.nama_motif);
		nama_motif.setTextSize(18f);
		deskripsi = (JustifiedTextView) findViewById(R.id.deskripsi);
		BitmapDrawable background = new BitmapDrawable (BitmapFactory.decodeResource(getResources(), R.drawable.hasil)); 
		background.setTileModeX(android.graphics.Shader.TileMode.REPEAT);
		getActionBar().setBackgroundDrawable(background);
		Bundle extras = getIntent().getExtras();
		byte[] byteArray = extras.getByteArray("bmp");
		match = extras.getInt("size");
		//Toast.makeText(getApplicationContext(), String.valueOf(match), Toast.LENGTH_LONG).show();
		if(match == 1){
			nama_motif.setText(R.string.kawung);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan di kalangan kerajaan<br/><b>Makna Filosofi</b><br/>Motif ini melambangkan harapan agar manusia selalu ingat akan asal-usulnya");
			nama_batik = "kawung picis";
		}else if(match == 2){
			nama_motif.setText(R.string.sidomukti_luhur);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara tujuh bulanan. Sebagai alat untuk menggendong bayi<br/><b>Makna Filosofi</b><br/>Sido Mukti = Gembira, kebahagiaan. Sehingga bayi yang di gendong merasa tenang dan mendapat kebahagiaan");
			nama_batik = "sido mukti luhur";
		}else if(match == 3){
			nama_motif.setText(R.string.grompol);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai diharapkan akan mempunyai banyak rejeki");
			nama_batik = "grompol";
		}else if(match == 4){
			nama_motif.setText(R.string.klitik);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan untuk dipakai di acara resmi<br/><b>Makna Filosofi</b><br/>Menunjukkan suatu kewibawaan");
			nama_batik = "klitik";
		}else if(match == 5){
			nama_motif.setText(R.string.kembang_metu_layar);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk berpergian dan untuk ber-pesta<br/><b>Makna Filosofi</b><br/>Kembang Temu = Kebapakan. Maka orang yang memakai akan memiliki sifat dewasa");
			nama_batik = "kembang metu layar";
		}else if(match == 6){
			nama_motif.setText(R.string.ksatrian);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai pengiring waktu upacara pengiringan pengantin<br/><b>Makna Filosofi</b><br/>Agar pemakai terlihat gagah dan memiliki sifat seperti ksatria");
			nama_batik = "ksatrian";
		}else if(match == 7){
			nama_motif.setText(R.string.lerek_parang);
			deskripsi.setText("<b>Kegunaan</b><br/>Mitoni, dipakai pestai<br/><b>Makna Filosofi</b><br/> Parang centung = wis ceta macak, kalau dipakai kelihatan cantik (macak)");
			nama_batik = "lerek parang";
		}else if(match == 8){
			nama_motif.setText(R.string.nagasari);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara temanten Jawa<br/><b>Makna Filosofi</b><br/>Apabila memakai kain tersebut kepada pengantin akan mendapatkan barokah (rezeki)");
			nama_batik = "naga sari";
		}else if(match == 9){
			nama_motif.setText(R.string.nitik_ketongkeng);
			deskripsi.setText("<b>Kegunaan</b><br/>Bebas<br/><b>Makna Filosofi</b><br/>Biasanya dipakai oleh orang tua untuk mendapatkan rejeki dan serasi");
			nama_batik = "nitik ketongkeng";
		}else if(match == 10){
			nama_motif.setText(R.string.nitik);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan pada acara resmi<br/><b>Makna Filosofi</b><br/>Orang yang memakai diharapkan menjadi bijaksana dan dapat menilai orang lain dengan tepat");
			nama_batik = "nitik";
		}else if(match == 11){
			nama_motif.setText(R.string.parang_barong);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai oleh Sultan atau Raja<br/><b>Makna Filosofi</b><br/>Kekuasaan atau kewibawaan seorang Sultan atau Raja");
			nama_batik = "parang barong";
		}else if(match == 12){
			nama_motif.setText(R.string.parang_bligon);
			deskripsi.setText("<b>Kegunaan</b><br/>Menghadiri pesta<br/><b>Makna Filosofi</b><br/>Parang Bligo = bentuk bulat berarti kemantapan hati.Kembang Randu = melambangkan si pemakai memiliki kemantapan dalam hidup dan banyak rejeki");
			nama_batik = "parang bligon";
		}else if(match == 13){
			nama_motif.setText(R.string.parang_curigo);
			deskripsi.setText("<b>Kegunaan</b><br/>Menghadiri pesta<br/><b>Makna Filosofi</b><br/>Curigo = Keris, Kepet = Isis. Pemakai diharapkan memiliki kecerdasan, kewibawaan, serta ketenangan");
			nama_batik = "parang curigo";
		}else if(match == 14){
			nama_motif.setText(R.string.parang_kusumo);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana pria dan wanita<br/><b>Makna Filosofi</b><br/>Parang Kusumo = Bangsawan, Mangkoro = Mahkota. Pemakai mendapat kedudukan, keluhuran dan dijauhkan dari mara bahaya");
			nama_batik = "parangkusumo";
		}else if(match == 15){ 
			nama_motif.setText(R.string.parang_tuding);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara tujuh bulanan<br/><b>Makna Filosofi</b><br/>Pemakai mendapatkan kedudukan yang baik, awet muda dan simpatik");
			nama_batik = "parang tuding";
		}else if(match == 16){
			nama_motif.setText(R.string.sapit_urang);
			deskripsi.setText("<b>Kegunaan</b><br/>Sebagai koleksi dari lingkungan keraton.<br/><b>Makna Filosofi</b><br/>Orang yang menggenakannya diharapkan mempunyai kepribadian yang baik dan hidupnya tidak sembrono");
			nama_batik = "sapit urang";
		}else if(match == 17){
			nama_motif.setText(R.string.sekarjagad);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan orang tua mempelai pada upacara<br/><b>Makna Filosofi</b><br/>Membuat hati gembira");
			nama_batik = "sekar jagad";
		}else if(match == 18){
			nama_motif.setText(R.string.sekar_polo);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk pakaian sehari-hari<br/><b>Makna Filosofi</b><br/>Pemakai akan dapat memberikan dorongan atau pengaruh terhadap orang lain");
			nama_batik = "sekar polo";
		}else if(match == 19){
			nama_motif.setText(R.string.semen_gurdo);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk pesta dan busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai mendapatkan berkah dan terlihat berwibawa");
			nama_batik = "semen gurdo";
		}else if(match == 20){
			nama_motif.setText(R.string.semen_kuncoro);
			deskripsi.setText("<b>Kegunaan</b><br/>Pakaian harian keraton<br/><b>Makna Filosofi</b><br/>Pemakai akan memancarkan kebahagiaan");
			nama_batik = "semen kuncoro";
		}else if(match == 21){
			nama_motif.setText(R.string.semen_romo_gurdo);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai terlihat menjadi lebih menarik");
			nama_batik = "semen romo gurdo";
		}else if(match == 22){
			nama_motif.setText(R.string.sido_asih);
			deskripsi.setText("<b>Kegunaan</b><br/>Bebas<br/><b>Makna Filosofi</b><br/>Pemakai disenangi banyak orang");
			nama_batik = "sido asih";
		}else if(match == 23){
			nama_motif.setText(R.string.slobog);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara kematian<br/><b>Makna Filosofi</b><br/>Melambangkan harapan agar arwah yang meninggal mendapatkan kemudahan dan kelancaran dalam perjalanan menghadap Tuhan");
			nama_batik = "slobog";
		}else if(match == 24){
			nama_motif.setText(R.string.tambal_kanoman);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai oleh golongan muda.<br/><b>Makna Filosofi</b><br/>Agar si pemakai terlihat serasi dan mendapatkan banyak rejeki");
			nama_batik = "tambal kanoman";
		}else if(match == 25){
			nama_motif.setText(R.string.tirta_terja);
			deskripsi.setText("<b>Kegunaan</b><br/>Pakaian<br/><b>Makna Filosofi</b><br/>Tirta = Air, Teja = Cahaya. Pemakai terlihat lebih bercahaya");
			nama_batik = "tirta terja";
		}else if(match == 26){
			nama_motif.setText(R.string.truntum_sri_kuncoro);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan oleh orang tua pengantin pada waktu temu pengantin<br/><b>Makna Filosofi</b><br/>Truntum = Menuntun. Sebagai orang tua berkewajiban menuntun kedua mempelai memasuki hidup baru yang banyak liku-liku");
			nama_batik = "truntum sri kuncoro";
		}else if(match == 27){
			nama_motif.setText(R.string.udan_liris);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan untuk dipakai di acara resmi<br/><b>Makna Filosofi</b><br/>Menunjukkan suatu kewibawaan");
			nama_batik = "udan liris";
		}else if(match == 28){
			nama_motif.setText(R.string.wahyu_tumurun_cantel);
			deskripsi.setText("<b>Kegunaan</b>\nDipakai pengantin pada waktu temu pengantin\n<b>Makna Filosofi</b>\nDengan menggunakan kedua kain ini, kedua pengantin diharapkan mendapatkan anugrah Tuhan berupa kehidupan yang bahagia dan sejahtera serta mendapatkan petunjuk");
			nama_batik = "wahyu tumurun cantel";
		}else if(match == 29){
			nama_motif.setText(R.string.wahyu_tumurun);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Agar si pemakai mendapatkan wahyu atau anugrah");
			nama_batik = "wahyu tumurun";
		}else if(match == 30){
			nama_motif.setText(R.string.ciptoning);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk acara resmi<br/><b>Makna Filosofi</b><br/>Pemakai menjadi orang bijak dan mampu memberikan petunjuk jalan yang benar");
			nama_batik = "ciptoning";
		}else if(match == 31){
			nama_motif.setText(R.string.kawung);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan di kalangan kerajaan<br/><b>Makna Filosofi</b><br/>Motif ini melambangkan harapan agar manusia selalu ingat akan asal-usulnya");
			nama_batik = "kawung picis";
		}
		bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
		img.setImageBitmap(bmp);
			// TODO Auto-generated catch block e.printStackTrace(); }
	    readmore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com/search?q="+nama_batik+"+batik&btnI=I'm+Feeling+Lucky")));
			}
		});
		save.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				saveImageToExternalStorage(bmp);
				Intent i = new Intent(Result.this, MainActivity.class);
				startActivity(i);

			}
		});
		fitting.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Result.this, Fitting.class);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				i.putExtra("bmp", byteArray);
				startActivity(i);
			}
		});
		delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Result.this, MainActivity.class);
				startActivity(i);
			}
		});
		
	    
	}
	
	public void saveImageToExternalStorage(Bitmap image) {
		String fullPath = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/all_batik";
		try {
			File dir = new File(fullPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			OutputStream fOut = null;
			String timeStamp = new SimpleDateFormat("yyD_HHmmss")
					.format(new Date());
			String imageFileName = nama_batik + "_" + timeStamp + ".jpg";
			String str = imageFileName;
			File file = new File(fullPath, imageFileName);
			if (file.exists())
				file.delete();
			file.createNewFile();
			fOut = new FileOutputStream(file);
			image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
			fOut.flush();
			fOut.close();
		} catch (Exception e) {
			Log.e("saveToExternalStorage()", e.getMessage());
		}
	}
	
	

}
