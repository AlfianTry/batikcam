package com.ugm.batikcam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleActivity extends Activity {

	ImageView img;
	Button btn;
	public Button readmore;
	public Button fitting,delete;
	JustifiedTextView deskripsi;
	Bitmap bmp;
	TextView nama;
	String nama_batik;
	String name;
	String kegunaan;
	String filosofi;
	String filename;
	JSONObject mJsonObj;
	
	
	String imagePath;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		img = (ImageView) findViewById(R.id.batik);
		readmore = (Button) findViewById(R.id.button2);
		fitting = (Button) findViewById(R.id.button3);
		delete = (Button) findViewById(R.id.button4);
		BitmapDrawable background = new BitmapDrawable (BitmapFactory.decodeResource(getResources(), R.drawable.detail)); 
		background.setTileModeX(android.graphics.Shader.TileMode.REPEAT);
		Bundle extras = getIntent().getExtras();
		
		getActionBar().setBackgroundDrawable(background);
		imagePath = getIntent().getStringExtra("imgpath");
		
		final File file = new File(imagePath);
		nama = (TextView) findViewById(R.id.nama_motif);
		nama.setTextSize(18f);
		deskripsi = (JustifiedTextView) findViewById(R.id.deskripsi);
		
		
		if(imagePath.contains("kawung")){
			nama.setText(R.string.kawung);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan di kalangan kerajaan<br/><b>Makna Filosofi</b><br/>Motif ini melambangkan harapan agar manusia selalu ingat akan asal-usulnya");
			nama_batik = "kawung picis";
		}else if(imagePath.contains("luhur")){
			nama.setText(R.string.sidomukti_luhur);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara tujuh bulanan. Sebagai alat untuk menggendong bayi<br/><b>Makna Filosofi</b><br/>Sido Mukti = Gembira, kebahagiaan. Sehingga bayi yang di gendong merasa tenang dan mendapat kebahagiaan");
			nama_batik = "sido mukti luhur";
		}else if(imagePath.contains("grompol")){
			nama.setText(R.string.grompol);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai diharapkan akan mempunyai banyak rejeki");
			nama_batik = "grompol";
		}else if(imagePath.contains("klitik")){
			nama.setText(R.string.klitik);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan untuk dipakai di acara resmi<br/><b>Makna Filosofi</b><br/>Menunjukkan suatu kewibawaan");
			nama_batik = "klitik";
		}else if(imagePath.contains("temu")){
			nama.setText(R.string.kembang_metu_layar);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk berpergian dan untuk ber-pesta<br/><b>Makna Filosofi</b><br/>Kembang Temu = Kebapakan. Maka orang yang memakai akan memiliki sifat dewasa");
			nama_batik = "kembang metu layar";
		}else if(imagePath.contains("ksatrian")){
			nama.setText(R.string.ksatrian);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai pengiring waktu upacara pengiringan pengantin<br/><b>Makna Filosofi</b><br/>Agar pemakai terlihat gagah dan memiliki sifat seperti ksatria");
			nama_batik = "ksatrian";
		}else if(imagePath.contains("lerek")){
			nama.setText(R.string.lerek_parang);
			deskripsi.setText("<b>Kegunaan</b><br/>Mitoni, dipakai pestai<br/><b>Makna Filosofi</b><br/> Parang centung = wis ceta macak, kalau dipakai kelihatan cantik (macak)");
			nama_batik = "lerek parang";
		}else if(imagePath.contains("sari")){
			nama.setText(R.string.nagasari);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara temanten Jawa<br/><b>Makna Filosofi</b><br/>Apabila memakai kain tersebut kepada pengantin akan mendapatkan barokah (rezeki)");
			nama_batik = "naga sari";
		}else if(imagePath.contains("ketongkeng")){
			nama.setText(R.string.nitik_ketongkeng);
			deskripsi.setText("<b>Kegunaan</b><br/>Bebas<br/><b>Makna Filosofi</b><br/>Biasanya dipakai oleh orang tua untuk mendapatkan rejeki dan serasi");
			nama_batik = "nitik ketongkeng";
		}else if(imagePath.contains("nitik")){
			nama.setText(R.string.nitik);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan pada acara resmi<br/><b>Makna Filosofi</b><br/>Orang yang memakai diharapkan menjadi bijaksana dan dapat menilai orang lain dengan tepat");
			nama_batik = "nitik";
		}else if(imagePath.contains("barong")){
			nama.setText(R.string.parang_barong);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai oleh Sultan atau Raja<br/><b>Makna Filosofi</b><br/>Kekuasaan atau kewibawaan seorang Sultan atau Raja");
			nama_batik = "parang barong";
		}else if(imagePath.contains("bligon")){
			nama.setText(R.string.parang_bligon);
			deskripsi.setText("<b>Kegunaan</b><br/>Menghadiri pesta<br/><b>Makna Filosofi</b><br/>Parang Bligo = bentuk bulat berarti kemantapan hati.Kembang Randu = melambangkan si pemakai memiliki kemantapan dalam hidup dan banyak rejeki");
			nama_batik = "parang bligon";
		}else if(imagePath.contains("curigo")){
			nama.setText(R.string.parang_curigo);
			deskripsi.setText("<b>Kegunaan</b><br/>Menghadiri pesta<br/><b>Makna Filosofi</b><br/>Curigo = Keris, Kepet = Isis. Pemakai diharapkan memiliki kecerdasan, kewibawaan, serta ketenangan");
			nama_batik = "parang curigo";
		}else if(imagePath.contains("kusumo")){
			nama.setText(R.string.parang_kusumo);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana pria dan wanita<br/><b>Makna Filosofi</b><br/>Parang Kusumo = Bangsawan, Mangkoro = Mahkota. Pemakai mendapat kedudukan, keluhuran dan dijauhkan dari mara bahaya");
			nama_batik = "parangkusumo";
		}else if(imagePath.contains("tuding")){ 
			nama.setText(R.string.parang_tuding);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara tujuh bulanan<br/><b>Makna Filosofi</b><br/>Pemakai mendapatkan kedudukan yang baik, awet muda dan simpatik");
			nama_batik = "parang tuding";
		}else if(imagePath.contains("sapit")){
			nama.setText(R.string.sapit_urang);
			deskripsi.setText("<b>Kegunaan</b><br/>Sebagai koleksi dari lingkungan keraton.<br/><b>Makna Filosofi</b><br/>Orang yang menggenakannya diharapkan mempunyai kepribadian yang baik dan hidupnya tidak sembrono");
			nama_batik = "sapit urang";
		}else if(imagePath.contains("jagad")){
			nama.setText(R.string.sekarjagad);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan orang tua mempelai pada upacara<br/><b>Makna Filosofi</b><br/>Membuat hati gembira");
			nama_batik = "sekar jagad";
			nama.setText(R.string.sekar_polo);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk pakaian sehari-hari<br/><b>Makna Filosofi</b><br/>Pemakai akan dapat memberikan dorongan atau pengaruh terhadap orang lain");
			nama_batik = "sekar polo";
		}else if(imagePath.contains("gurdo")){
			nama.setText(R.string.semen_gurdo);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk pesta dan busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai mendapatkan berkah dan terlihat berwibawa");
			nama_batik = "semen gurdo";
		}else if(imagePath.contains("kuncoro")){
			nama.setText(R.string.semen_kuncoro);
			deskripsi.setText("<b>Kegunaan</b><br/>Pakaian harian keraton<br/><b>Makna Filosofi</b><br/>Pemakai akan memancarkan kebahagiaan");
			nama_batik = "semen kuncoro";
		}else if(imagePath.contains("romo")){
			nama.setText(R.string.semen_romo_gurdo);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Pemakai terlihat menjadi lebih menarik");
			nama_batik = "semen romo gurdo";
		}else if(imagePath.contains("asih")){
			nama.setText(R.string.sido_asih);
			deskripsi.setText("<b>Kegunaan</b><br/>Bebas<br/><b>Makna Filosofi</b><br/>Pemakai disenangi banyak orang");
			nama_batik = "sido asih";
		}else if(imagePath.contains("slobog")){
			nama.setText(R.string.slobog);
			deskripsi.setText("<b>Kegunaan</b><br/>Upacara kematian<br/><b>Makna Filosofi</b><br/>Melambangkan harapan agar arwah yang meninggal mendapatkan kemudahan dan kelancaran dalam perjalanan menghadap Tuhan");
			nama_batik = "slobog";
		}else if(imagePath.contains("tambal")){
			nama.setText(R.string.tambal_kanoman);
			deskripsi.setText("<b>Kegunaan</b><br/>Dipakai oleh golongan muda.<br/><b>Makna Filosofi</b><br/>Agar si pemakai terlihat serasi dan mendapatkan banyak rejeki");
			nama_batik = "tambal kanoman";
		}else if(imagePath.contains("teja")){
			nama.setText(R.string.tirta_terja);
			deskripsi.setText("<b>Kegunaan</b><br/>Pakaian<br/><b>Makna Filosofi</b><br/>Tirta = Air, Teja = Cahaya. Pemakai terlihat lebih bercahaya");
			nama_batik = "tirta terja";
		}else if(imagePath.contains("truntum")){
			nama.setText(R.string.truntum_sri_kuncoro);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan oleh orang tua pengantin pada waktu temu pengantin<br/><b>Makna Filosofi</b><br/>Truntum = Menuntun. Sebagai orang tua berkewajiban menuntun kedua mempelai memasuki hidup baru yang banyak liku-liku");
			nama_batik = "truntum sri kuncoro";
		}else if(imagePath.contains("udan")){
			nama.setText(R.string.udan_liris);
			deskripsi.setText("<b>Kegunaan</b><br/>Digunakan untuk dipakai di acara resmi<br/><b>Makna Filosofi</b><br/>Menunjukkan suatu kewibawaan");
			nama_batik = "udan liris";
		}else if(imagePath.contains("wahyu tumurun cantel")){
			nama.setText(R.string.wahyu_tumurun_cantel);
			deskripsi.setText("<b>Kegunaan</b>\nDipakai pengantin pada waktu temu pengantin\n<b>Makna Filosofi</b>\nDengan menggunakan kedua kain ini, kedua pengantin diharapkan mendapatkan anugrah Tuhan berupa kehidupan yang bahagia dan sejahtera serta mendapatkan petunjuk");
			nama_batik = "wahyu tumurun cantel";
		}else if(imagePath.contains("wahyu tumurun")){
			nama.setText(R.string.wahyu_tumurun);
			deskripsi.setText("<b>Kegunaan</b><br/>Busana daerah<br/><b>Makna Filosofi</b><br/>Agar si pemakai mendapatkan wahyu atau anugrah");
			nama_batik = "wahyu tumurun";
		}else if(imagePath.contains("ciptoning")){
			nama.setText(R.string.ciptoning);
			deskripsi.setText("<b>Kegunaan</b><br/>Untuk acara resmi<br/><b>Makna Filosofi</b><br/>Pemakai menjadi orang bijak dan mampu memberikan petunjuk jalan yang benar");
			nama_batik = "ciptoning";
		}else if(imagePath.contains("update")){

			AsyncHttpClient client = new AsyncHttpClient();
			client.get("http://alfiantriplek.url.ph/get.php", new AsyncHttpResponseHandler() {
				
				@Override
				public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
					String str=null;
					try {
						str = new String(arg2, "UTF-8");
						try{
							JSONObject mJsonObj = new JSON(str).getJSONObject(0);
							name = mJsonObj.getString("nama");
							kegunaan = mJsonObj.getString("kegunaan");
							filosofi = mJsonObj.getString("filosofi");
							filename = mJsonObj.getString("filename");
						}
						catch(JSONException e){
							e.getMessage();
						}
						nama.setText(name);
						deskripsi.setText("<b>Kegunaan</b><br/>"+kegunaan+"<br/><b>Makna Filosofi</b><br/>"+filosofi);
						nama_batik = name;
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Log.e("TES",str);
					
					
				}
				
				@Override
				public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
					Log.e("GAGAL","");
					
				}
			});
			
		}
		
		
		
		
		img.setImageURI(Uri.fromFile(file));
		try {
			bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			// TODO Auto-generated catch block e.printStackTrace(); }
		 readmore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com/search?q="+nama_batik+"+batik&btnI=I'm+Feeling+Lucky")));
				}
			});

		fitting.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SingleActivity.this, Fitting.class);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				i.putExtra("bmp", byteArray);
				startActivity(i);
			}
		});
		
		delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				file.delete();
				startActivity(new Intent(SingleActivity.this,MainActivity.class));
			}
		});
	    //more code of yours
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.single, menu);

		// Locate MenuItem with ShareActionProvider
		MenuItem item = menu.findItem(R.id.menu_item_share);
		// Return true to display menu
		return false;

	}
	
	private class LongRunningGetIO extends AsyncTask <Void, Void, String> {
		protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n>0) {
				byte[] b = new byte[4096];
				n =  in.read(b);
				if (n>0) out.append(new String(b, 0, n));
			}
			return out.toString();
		}

		@Override
		protected String doInBackground(Void... params) {
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGet = new HttpGet("http://alfiantriplek.url.ph/get.php/");
			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet, localContext);
				HttpEntity entity = response.getEntity();
				text = getASCIIContentFromEntity(entity);
				Log.d("do in bg", text);
			} 
			catch (Exception e) {
				return e.getLocalizedMessage();
			}

			return text;
		}
		
		@Override
		protected void onPostExecute(String results) {
			if (results!=null) {
				try{
					mJsonObj = new JSONObject(results);
					name = mJsonObj.getString("nama");
					kegunaan = mJsonObj.getString("kegunaan");
					filosofi = mJsonObj.getString("filosofi");
					filename = mJsonObj.getString("filename");
					Log.e("TAG", name+kegunaan+filosofi+filename);
				}
				catch(JSONException e){
					e.getMessage();
				}
				
			}
			
			nama.setText(name);
			deskripsi.setText("<b>Kegunaan</b><br/>"+kegunaan+"<br/><b>Makna Filosofi</b><br/>"+filosofi);
			nama_batik = "ciptoning";
		}

	}
	
	

}
